import React from 'react';
import { SafeAreaView, AppRegistry, } from 'react-native';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';
import AppRouter from './router';
import { store, persistor } from './configureStore';

console.disableYellowBox = true;

class root extends React.Component {

  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <SafeAreaView style={{ flex: 1, backgroundColor: '#FFF' }}>
            <AppRouter />
          </SafeAreaView>
        </PersistGate>
      </Provider>
    );
  }
}

export default root;
AppRegistry.registerComponent('root', () => root);
