import { store } from '../configureStore';
import {
    defaultHeaders,
    buildResponse,
    BASE_URL,
    buildHeadersImage,
} from './apiHelper';


/* API FOR LOGIN */
export function login(username, password) {
    const user = encodeURIComponent(username);
    const pass = encodeURIComponent(password);
    const requestBody = `username=${user}&password=${pass}`;
    return fetch(`${BASE_URL}authenticate`, {
        headers: defaultHeaders,
        method: 'POST',
        body: requestBody,
    })
        .then(response => buildResponse(response, "ERROR LOGIN"))
        .then(data => data);
}
/* END API FOR LOGIN */
export function create(token, first_name, last_name, gender, email, avatar) {
    const fname = encodeURIComponent(first_name);
    const lname = encodeURIComponent(last_name);
    const gen = encodeURIComponent(gender);
    const em = encodeURIComponent(email);
    const av = encodeURIComponent(avatar);
    const requestBody = `first_name=${fname}&last_name=${lname}&email=${em}&gender=${gen}&avatar=${av}`;
    return fetch(`${BASE_URL}create?token=${token}`, {
        headers: defaultHeaders,
        method: 'PUT',
        body: requestBody
    })
        .then(response => buildResponse(response, "ERROR UPLOAD CONTACT"))
        .then(data => data)
}

export function agentlist(token) {
    return fetch(`${BASE_URL}contacts?token=${token}`, {
        method: 'GET',
        headers: defaultHeaders
    })
        .then(response => buildResponse(response, "ERROR COLLECTION BANK LIST"))
        .then(data => data)
}








