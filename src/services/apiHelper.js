import { Actions } from 'react-native-router-flux';
import { store } from '../configureStore';
import Storage from '../data/Storage';
import { ERROR_TAG, receiveError, DEFAULT_ERROR_MESSAGE } from '../redux/error';

export const SITE_URL = 'https://devel-7.tonjoostudio.com/recruitment-api/';
export const BASE_URL = `${SITE_URL}`;
export const defaultHeaders = {
    'Content-Type': 'application/x-www-form-urlencoded'
};

export const multiPart = {
    'Content-Type': 'application/x-www-form-urlencoded'
};

export function buildHeaders(accessToken) {
    return {
        ...defaultHeaders,
        Authorization: `Bearer ${accessToken}`,
        Accept: 'application/json',
    };
}

export function buildHeadersImage(accessToken) {
    return {
        ...multiPart,
        Authorization: `Bearer ${accessToken}`,
        // Accept: 'application/json',
    };
}


export function buildResponse(response, errorTag) {
    if (!response.ok) {
        if (response.status === 401) {
            Actions.reset('Login')
        }
        store.dispatch(
            receiveError({
                errorCode: response.status,
                errorTag: errorTag,
                errorMessage: '',
                defaultMessage: 'Failed',
            }),
        );
    }

    return response.json();
}

export function buildResponseNew(response) {
    if (!response.ok) {
        if (response.status === 401) {
            Actions.reset('Login')
        }
    }
    return response.json();
}

export function buildResponseLogin(response, errorTag) {
    if (!response.ok) {
        if (response.status === 401 || response.status === 400) {
            store.dispatch(
                receiveError({
                    errorCode: response.status,
                    errorTag: errorTag,
                    defaultMessage: DEFAULT_ERROR_MESSAGE.DEFAULT_MESSAGE_1,
                }),
            );
        } else {
            store.dispatch(
                receiveError({
                    errorCode: response.status,
                    errorTag: errorTag,
                    errorMessage: DEFAULT_ERROR_MESSAGE.ERROR_MESSAGE_500,
                    defaultMessage: DEFAULT_ERROR_MESSAGE.DEFAULT_MESSAGE_2,
                }),
            );
        }
    }
    return response.json();
}
