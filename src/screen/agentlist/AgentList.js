import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, FlatList, TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import assets from '../../assets';
import HeaderBack from '../../components/HeaderBack';
import {
    fetchContacts, fetchContactsFinish, fetchContactsSuccess,
    getIsFetching, getData, getIsSuccess
} from '../../redux/contacts'
import { connect } from 'react-redux'
import { getDataAgentOffline } from '../../redux/offline/agent';

class AgentList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selfie: '',
            listagent: [],
            dataAgent: []
        }
    }

    async componentDidMount() {
        const { dispatch, data, dataAgent } = this.props
        // alert(JSON.stringify(dataAgent))
        dispatch(fetchContacts())
        this.setState({
            listagent: data.data,
            dataAgent: dataAgent
        })
    }

    async UNSAFE_componentWillReceiveProps(nextProps) {
        const { dispatch, isSuccess, data, dataAgent } = nextProps
        if (isSuccess) {
            this.setState({
                listagent: data.data
            });

            dispatch(fetchContactsFinish())
        }
    }

    render() {
        const { listagent, dataAgent } = this.state
        return (
            <View style={[styles.viewContainer, { backgroundColor: 'white' }]} >
                <HeaderBack
                    tittle={'Agents List'}
                />
                {dataAgent.length > 0 &&
                    <FlatList
                        data={dataAgent}
                        renderItem={({ item }) => (
                            <View style={{
                                flexDirection: 'row',
                                marginTop: hp(2),
                                paddingVertical: hp(2),
                                marginHorizontal: wp(5),
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 2,
                                },
                                shadowOpacity: 0.25,
                                shadowRadius: 3.84,
                                elevation: 5,
                                backgroundColor: 'white'
                            }}>
                                <View style={{ flex: 1 }}>
                                    <Image
                                        source={{ uri: item.avatar }}
                                        style={{
                                            width: wp(20),
                                            height: wp(15),
                                            alignSelf: 'center',
                                            justifyContent: 'center'
                                            // backgroundColor: 'red'
                                        }}
                                    />
                                </View>
                                <View style={{ flex: 2 }}>
                                    <Text>{item.first_name} {item.last_name}</Text>
                                    <Text>{item.gender}</Text>
                                    <Text>{item.email}</Text>
                                </View>
                                <View style={{ flex: 1 }}>
                                    <Image
                                        source={assets.restart}
                                        style={{
                                            position: 'absolute',
                                            right: wp(5),
                                            width: wp(5),
                                            height: wp(5),
                                            // backgroundColor: 'red'
                                        }}
                                    />
                                </View>
                            </View>
                        )}
                        keyExtractor={item => item.id}
                    />}
                <FlatList
                    data={listagent}
                    renderItem={({ item }) => (
                        <View style={{
                            flexDirection: 'row',
                            marginTop: hp(2),
                            paddingVertical: hp(2),
                            marginHorizontal: wp(5),
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 2,
                            },
                            shadowOpacity: 0.25,
                            shadowRadius: 3.84,
                            elevation: 5,
                            backgroundColor: 'white'
                        }}>
                            <View style={{ flex: 1 }}>
                                <Image
                                    source={{ uri: item.avatar }}
                                    style={{
                                        width: wp(20),
                                        height: wp(15),
                                        alignSelf: 'center',
                                        justifyContent: 'center'
                                        // backgroundColor: 'red'
                                    }}
                                />
                            </View>
                            <View style={{ flex: 2 }}>
                                <Text>{item.first_name} {item.last_name}</Text>
                                <Text>{item.gender}</Text>
                                <Text>{item.email}</Text>
                            </View>
                            <View style={{ flex: 1 }}>
                                <Image
                                    source={assets.save}
                                    style={{
                                        position: 'absolute',
                                        right: wp(5),
                                        width: wp(5),
                                        height: wp(5),
                                        // backgroundColor: 'red'
                                    }}
                                />
                            </View>
                        </View>
                    )}
                    keyExtractor={item => item.id}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    viewContainer: {
        flex: 1,
        backgroundColor: '#fff',
    },
    viewWrapper: {
        flex: 1,
        paddingHorizontal: wp(10),
        backgroundColor: '#fff',
        justifyContent: 'center',
    },
    textTitle: {
        fontSize: wp(7),
        marginTop: hp(5),
        textAlign: 'center',
        fontWeight: 'bold',
        color: '#E56717',
    },
});

const mapStateToProps = state => ({
    isFetching: getIsFetching(state.contact),
    data: getData(state.contact),
    isSuccess: getIsSuccess(state.contact),
    // OFFLINE DATA
    dataAgent: getDataAgentOffline(state.agent)
})

export default connect(mapStateToProps)(AgentList)