import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity, Picker } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import assets from '../../assets';
import HeaderBack from '../../components/HeaderBack';
import Input from '../../components/Input';
import * as ImagePicker from "react-native-image-picker"
import Button from '../../components/Button';
import { connect } from 'react-redux';
import { fetchCreate } from '../../redux/create';
import Storage from "../../data/Storage";
import { getDataAgentOffline, setAgentOffline } from '../../redux/offline/agent';

class AddAgent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedValue: 'male',
            selfie: '',
            gender: 'male',
            first_name: '',
            last_name: '',
            email: '',
            avatar: ''
        }
    }

    chooseFile = () => {
        var options = {
            title: 'avatar',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
            quality: 0.15
        };
        ImagePicker.launchCamera(options, response => {
            console.log('Response = ', response);
            if (response.didCancel) {
                // console.log('User cancelled image picker');
            } else if (response.error) {
                // console.log('ImagePicker Error: ', response.error);
            } else {
                let source = response;
                this.setState({
                    avatar: source.uri,
                    selfie: source,
                });
            }
        });
    };

    async componentDidMount() {
        const { dataAgentOffline } = this.props
        await Storage.setDataAgent(dataAgentOffline);
    }

    formValidation() {
        const { first_name, last_name, email, gender, avatar } = this.state
        if (first_name === '') {
            alert('First Name is rquired')
            return false;
        }
        if (last_name === '') {
            alert('Last Name is rquired')
            return false;
        }
        if (email === '') {
            alert('email is rquired')
            return false;
        }
        if (gender === '') {
            alert('gender is rquired')
            return false;
        }
        if (avatar === '') {
            alert('Avatar is rquired')
            return false;
        }
        return true;
    }



    async onSubmit() {
        const { dispatch } = this.props
        const { first_name, last_name, email, gender, avatar } = this.state
        if (this.formValidation()) {
            await dispatch(fetchCreate({ first_name, last_name, email, gender, avatar }))
        }
    }

    async onSave() {
        const dataOffline = await Storage.getDataAgent();
        const { dispatch } = this.props
        const { first_name, last_name, email, gender, avatar } = this.state
        if (this.formValidation()) {
            await dispatch(setAgentOffline({
                data: {
                    'first_name': first_name,
                    'last_name': last_name,
                    'email': email,
                    'gender': gender,
                    'avatar': avatar,
                }, dataOffline
            }))
        }
    }


    render() {
        return (
            <View style={styles.viewContainer} >
                <HeaderBack
                    tittle={'Add Agent'}
                />
                <View>
                    <Input
                        title={'First Name'}
                        onChangeText={(text) => this.setState({
                            first_name: text
                        })}
                    />
                    <Input
                        title={'Last Name'}
                        onChangeText={(text) => this.setState({
                            last_name: text
                        })}
                    />
                    {/* PICKER */}
                    <View style={{
                        flexDirection: 'row',
                        marginTop: hp(1),
                        paddingHorizontal: wp(5)
                    }}>
                        <Text style={{
                            flex: 1,
                            alignSelf: 'center'
                        }}>Gender</Text>
                        <View style={{
                            borderWidth: 1,
                            flex: 2.5,
                            paddingHorizontal: wp(5),
                        }}>
                            <Picker
                                selectedValue={this.state.selectedValue}
                                style={{
                                    height: hp(5),

                                }}
                                onValueChange={(itemValue, itemIndex) => this.setState({
                                    selectedValue: itemValue,
                                    gender: itemValue
                                })}
                            >
                                <Picker.Item label="MALE" value="male" />
                                <Picker.Item label="FEMALE" value="female" />
                            </Picker>
                        </View>
                    </View>
                    {/* END PICKER */}
                    <Input
                        title={'Email'}
                        onChangeText={(text) => this.setState({
                            email: text
                        })}
                    />

                    {/* ADD PHOTO BY CAMERA */}
                    <View style={{
                        flexDirection: 'row',
                        marginTop: hp(1),
                        paddingHorizontal: wp(5)
                    }}>
                        <Text style={{
                            flex: 1,
                        }}>Photo</Text>
                        {this.state.selfie == '' ?
                            <TouchableOpacity
                                onPress={this.chooseFile.bind(this)}
                                style={{
                                    flex: 2.5,
                                    height: hp(20),
                                    borderWidth: 1,
                                    backgroundColor: 'red',
                                    paddingHorizontal: wp(5),
                                }}
                            >
                                <Image
                                    source={assets.icon_camera}
                                    style={{
                                        flex: 2.5,
                                        height: hp(20),
                                        borderWidth: 1,
                                        backgroundColor: 'red',
                                        paddingHorizontal: wp(5),
                                    }}
                                />
                            </TouchableOpacity> :

                            <TouchableOpacity
                                onPress={() => this.chooseFile()}
                                style={{
                                    flex: 2.5,
                                    height: hp(20),
                                    borderWidth: 1,
                                    // backgroundColor: 'red',
                                    paddingHorizontal: wp(5),
                                }}
                            >
                                <Image
                                    source={this.state.selfie}
                                    style={{
                                        width: wp(50),
                                        height: hp(20),
                                        borderWidth: 1,
                                        resizeMode: 'contain',
                                        alignSelf: 'center',
                                        paddingHorizontal: wp(5),
                                    }}
                                />
                            </TouchableOpacity>

                        }
                        {/* END PHOTO BY CAMERA */}
                    </View>
                </View>
                <View style={{
                    paddingHorizontal: wp(5),
                    flexDirection: 'row',
                    justifyContent: 'space-between'
                }}>
                    <Button
                        onPress={() => this.onSave()}
                        title={'Save as Draft'}
                    />
                    <Button
                        onPress={() => this.onSubmit()}
                        title={'Submit'}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    viewContainer: {
        flex: 1,
        backgroundColor: '#fff',
    },
    viewWrapper: {
        flex: 1,
        paddingHorizontal: wp(10),
        backgroundColor: '#fff',
        justifyContent: 'center',
    },
    textTitle: {
        fontSize: wp(7),
        marginTop: hp(5),
        textAlign: 'center',
        fontWeight: 'bold',
        color: '#E56717',
    },
});

const mapStateToProps = state => ({
    // OFFLINE MODE
    dataAgentOffline: getDataAgentOffline(state.agent)
})

export default connect(mapStateToProps)(AddAgent)
