import React from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import assets from '../../assets';
import Menu from '../../components/menu';

export default function Home() {
    return (
        <View style={styles.viewContainer}>
            <Text style={styles.textTitle}>Tonjoo Test</Text>
            <View style={styles.viewWrapper}>
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between'
                }}>
                    <Menu
                        onPress={() => Actions.AgentList()}
                        img={assets.menu_agent_list}
                        menuTittle={'Agents List'}
                    />

                    <Menu
                        onPress={() => Actions.AddAgent()}
                        img={assets.menu_add_agent}
                        menuTittle={'Add New Agents'}
                    />
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    viewContainer: {
        flex: 1,
        backgroundColor: '#fff',
    },
    viewWrapper: {
        flex: 1,
        paddingHorizontal: wp(10),
        backgroundColor: '#fff',
        justifyContent: 'center',
    },
    textTitle: {
        fontSize: wp(7),
        marginTop: hp(5),
        textAlign: 'center',
        fontWeight: 'bold',
        color: '#E56717',
    },
});
