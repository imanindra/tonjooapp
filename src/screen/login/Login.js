import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import {
    login,
    loginFinish,
    getIsSuccess,
    getIsFetching,
    getMessage,
} from '../../redux/auth';
import {
    getHasError,
    receiveError,
    getErrorMessage,
    getDefaultMessage,
    setErrorMessage,
} from '../../redux/error';



class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
        };
    }

    async componentDidMount() {

    }

    onLogin() {
        const { dispatch } = this.props;
        const { username, password } = this.state;
        this.setState({
            isLoading: true
        })
        if (username === '') {
            this.setState({
                isLoading: false
            })
            alert('The username field is required.')
        } else if (password === '') {
            this.setState({
                isLoading: false
            })
            alert('The password field is required.')
        }
        else {
            dispatch(login({ username, password }));
            this.setState({
                isLoading: false
            })
        }
    }


    render() {
        return (
            <View style={styles.viewContainer} >
                <Text style={styles.textTitle}>Tonjoo Test</Text>
                <View style={styles.viewWrapper}>
                    <View style={{
                        paddingHorizontal: wp(5)
                    }}>
                        <Text style={styles.vTextInput}>Username</Text>
                        <TextInput
                            onChangeText={(text) => this.setState({ username: text })}
                            style={styles.textInput} />
                    </View>

                    <View style={{
                        paddingHorizontal: wp(5)
                    }}>
                        <Text style={styles.vTextInput}>Password</Text>
                        <TextInput
                            onChangeText={(text) => this.setState({ password: text })}
                            secureTextEntry
                            style={styles.textInput} />
                    </View>

                    <TouchableOpacity
                        onPress={() => this.onLogin()}
                    >
                        <View style={styles.viewButton}>
                            <Text style={styles.textLogin}>Submit</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    viewContainer: {
        flex: 1,
        backgroundColor: '#fff',
    },
    viewWrapper: {
        flex: 1,
        paddingHorizontal: wp(10),
        backgroundColor: '#fff',
        justifyContent: 'center',
    },
    vTextInput: {
        fontSize: wp('5%'),
        marginBottom: hp(-2)
    },
    textInput: {
        width: wp(70),
        height: hp(7),
        borderWidth: 1,
        paddingHorizontal: wp(5),
        borderColor: '#DDD',
        borderRadius: 10,
        alignSelf: 'center',
        marginVertical: hp(2.33),
    },
    viewButton: {
        width: wp(70),
        height: hp(7),
        alignItems: 'center',
        backgroundColor: '#E56717',
        justifyContent: 'center',
        alignSelf: 'center',
        borderRadius: 10,
        marginTop: hp(1.1),
    },
    textLogin: {
        fontWeight: 'bold',
        color: '#FFF',
        fontSize: wp(5),
    },
    textTitle: {
        fontSize: wp(7),
        marginTop: hp(5),
        textAlign: 'center',
        fontWeight: 'bold',
        color: '#E56717',
    },
});

const mapStateToProps = state => ({
    // isFetching: getIsFetching(state.auth),
    // isSuccess: getIsSuccess(state.auth),
    // hasError: getHasError(state.error),
    // errorMessage: getErrorMessage(state.error),
});


export default connect(mapStateToProps)(Login);