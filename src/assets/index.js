export default {
    menu_agent_list: require('./ic-agent-list.png'),
    menu_add_agent: require('./ic-add-new-agents.png'),
    restart: require('./ic-restart.png'),
    save: require('./ic-save.png'),
    back: require('./ic-previous.png')
}