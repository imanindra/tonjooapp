import React, { Component } from 'react';
import { View, } from 'react-native';
import { Scene, Router } from 'react-native-router-flux';
import Home from './screen/home/Home';
import Login from './screen/login/Login';
import AgentList from './screen/agentlist/AgentList';
import AddAgent from './screen/addagent/AddAgent';

class router extends React.Component {
    state = {
        isInitiated: false,
        token: null,
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Router>
                    <Scene key="root">
                        <Scene key="Login" initial={true} component={Login} hideNavBar />
                        <Scene key="Home" component={Home} hideNavBar />
                        <Scene key="AgentList" component={AgentList} hideNavBar />
                        <Scene key="AddAgent" component={AddAgent} hideNavBar />
                    </Scene>
                </Router>
            </View>
        )
    }
}

export default router;
