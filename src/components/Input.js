import React from 'react'
import {
    StyleSheet,
    Text,
    View,
    TextInput
} from 'react-native'
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from 'react-native-responsive-screen';


const Input = ({ title, onChangeText }) => {
    return (
        <View style={{
            flexDirection: 'row',
            marginTop: hp(1),
            paddingHorizontal: wp(5)
        }}>
            <Text style={{
                flex: 1,
                alignSelf: 'center'
            }}>{title}</Text>
            <TextInput
                style={{
                    flex: 2.5,
                    height: hp(5),
                    borderWidth: 1,
                    paddingHorizontal: wp(5),
                }}
                onChangeText={onChangeText}
                placeholder={title}
            />
        </View>
    )
}

const styles = StyleSheet.create({
});


export default Input
