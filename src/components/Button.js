import React from 'react'
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity
} from 'react-native'
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from 'react-native-responsive-screen';


const Button = ({ title, onPress }) => {
    return (
        <TouchableOpacity
            onPress={onPress}
            style={{
                height: wp(10),
                paddingHorizontal: wp(5),
                borderRadius: wp(1),
                marginTop: hp(2),
                backgroundColor: 'red'
            }}>
            <Text style={{
                flex: 1,
                textAlignVertical: 'center',
                // backgroundColor: 'red',
                alignSelf: 'center'
            }}>{title}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
});


export default Button
