import React from 'react'
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    Image
} from 'react-native'
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from 'react-native-responsive-screen';


const Menu = ({ img, menuTittle, onPress }) => {
    return (
        <TouchableOpacity
            onPress={onPress}>
            <Image
                source={img}
                style={{
                    width: wp(30),
                    height: hp(15),
                    resizeMode: 'contain'
                }}
            />
            <Text style={{ textAlign: 'center' }}>{menuTittle}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
});


export default Menu
