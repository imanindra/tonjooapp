import React from 'react'
import {
    StyleSheet,
    Image,
    TouchableOpacity,
    View,
    Text,
    TextInput
} from 'react-native'
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import assets from '../assets';
// import Font from '../constants/Font';


const HeaderBack = ({ languagePress, tittle, nameLocation, locationPress, flag }) => {
    return (
        <View style={styles.vContainer}>
            <View style={{ flexDirection: 'row' }}>
                <View
                    style={{
                        flex: 1,
                        justifyContent: 'center'

                    }}
                >
                    <TouchableOpacity
                        onPress={()=> Actions.pop()}
                        style={{ flexDirection: 'row' }}
                    >
                        <Image
                            source={assets.back}
                            resizeMode='contain'
                            style={{
                                width: wp(7),
                                height: wp(7),
                                marginRight: wp(1),
                                alignSelf: 'center',
                                // backgroundColor: 'red'
                            }}
                        />
                        <Text style={{ textAlignVertical: 'center', color: 'white', fontWeight: 'bold' }}>{tittle}</Text>
                    </TouchableOpacity>
                </View>
            </View>

        </View >
    )
}

const styles = StyleSheet.create({
    vContainer: {
        width: wp(100),
        height: hp(7),
        backgroundColor: '#84BB41',
        justifyContent: 'center',
        paddingHorizontal: wp(3),
    },
    input: {
        height: 40,
        width: wp(30),
        margin: 12,
        borderWidth: 1,
        borderColor: 'white'
    },
    VBtnEat: {
        borderWidth: 1,
        paddingHorizontal: wp(1),
        paddingVertical: hp(1),
        borderRadius: wp(5),
        marginLeft: wp(-3),
        marginRight: wp(0),
        borderColor: 'white'
    },
    vBtnCalories: {
        borderWidth: 1,
        paddingHorizontal: wp(1),
        paddingVertical: hp(1),
        borderRadius: wp(5),
        borderColor: 'white'
    }
});


export default HeaderBack
