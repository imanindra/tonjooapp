import { StyleSheet ,Dimensions,Platform } from 'react-native'
import constant from '../../constant'
import { scale, verticalScale } from 'react-native-size-matters';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF'
  },
  formContainer: {
    marginTop: hp('3.58%'),
    paddingHorizontal: wp('10%'),
    backgroundColor: '#FFF',
  },
  textInputBox : {
    paddingLeft: wp('3%'),
    color: 'white', 
    fontWeight: '900',
    fontSize: wp('5%'),
  },
  textLabel: {
    fontSize: wp('5%'),
    fontFamily : 'Montserrat-Light'
  },
  textLabelDisable:{
    fontSize: wp('5%'),
    fontFamily : 'Montserrat-Light'
  },
  textButton: {
    fontSize: wp('5%'),
    fontFamily : 'Montserrat-SemiBold'
  },
  textButtonDisable: {
    fontSize: wp('5%'),
    fontFamily : 'Montserrat-SemiBold'
  },
  labelPasswordContainer: {
    width: '100%',
  },
  textForgotPassword: {
    position: 'absolute',
    right: 0,
    color: '#AAAAAA',
  },
  fieldContainer: {
    width: '100%',
    height: hp('7.5%'),
    borderRadius: scale(7),
    marginTop: height * 0.01343,
    marginBottom: height * 0.031,
    justifyContent: 'center',
    paddingLeft: scale(10),
    paddingRight: scale(10),
  },
  buttonRight:{
   alignSelf : 'flex-end'
  }
})