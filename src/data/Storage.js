import AsyncStorage from '@react-native-community/async-storage';

const TOKEN_DATA_KEY = 'TOKEN_DATA';
const DATA_AGENT = 'DATA_AGENT';

export default class Storage {

    // ADD TOKEN
    static async setToken(token) {
        try {
            await AsyncStorage.setItem(TOKEN_DATA_KEY, token);
        } catch (error) {
            throw new Error('Error While Saving Token');
        }
    }

    static async resetToken() {
        try {
            await AsyncStorage.removeItem(TOKEN_DATA_KEY);
        } catch (error) {
            throw new Error('error while reseting token');
        }
    }

    static async getToken() {
        try {
            const accessToken = await AsyncStorage.getItem(TOKEN_DATA_KEY);
            return accessToken;
        } catch (error) {
            throw new Error('Error While getting Token');
        }
    }
    // END TOKEN

    // ADD OFFLINE DATA AGENT

    static async setDataAgent(agent) {
        try {
            await AsyncStorage.setItem(DATA_AGENT, JSON.stringify(agent));
        } catch (error) {
            throw new Error('Error While Saving User Agent');
        }
    }

    static async getDataAgent() {
        try {
            const DATA = await AsyncStorage.getItem(DATA_AGENT);
            return JSON.parse(DATA);
        } catch (error) {
            throw new Error('Error While Getting User Agent');
        }
    }

    static async resetDataAgent() {
        try {
            await AsyncStorage.removeItem(DATA_AGENT);
        } catch {
            throw new Error('Error while Reseting User Agent');
        }
    }
    // END OFFLINE DATA AGENT

}
