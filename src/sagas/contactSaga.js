import { call, put, fork, take } from 'redux-saga/effects';
import {
    FETCH_CONTACTS,
    fetchContactsStart,
    fetchContactsFinish,
    fetchContactsSuccess,
} from '../redux/contacts';
import { setErrorMessage } from '../redux/error';
import { agentlist } from '../services/api';
import Storage from '../data/Storage';

function* contactSaga() {
    while (true) {
        const action = yield take(FETCH_CONTACTS);
        try {
            yield put(fetchContactsStart());
            const token = yield Storage.getToken();
            const agent = yield call(
                agentlist,
                token
            );
            yield put(fetchContactsSuccess(agent))

        } catch (err) {
            console.log('ERROR GET CONTACT LIST AGENT  ', err);
            fetchContactsFinish(err)
        }
    }
}

export default function* wacher() {
    yield fork(contactSaga);
}
