import { Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { call, put, fork, take } from 'redux-saga/effects';
import {
    FETCH_CREATE,
    fetchCreateStart,
    fetchCreateFinish,
    fetchCreateSuccess,
} from '../redux/create';
import Storage from '../data/Storage';
import { create } from '../services/api';

function* createSaga() {
    while (true) {
        const action = yield take(FETCH_CREATE);
        try {
            yield put(fetchCreateStart());
            const { first_name, last_name, gender, email, avatar } = action.payload;
            const token = yield Storage.getToken();
            const datacreate = yield call(
                create,
                token,
                first_name,
                last_name,
                gender,
                email,
                avatar
            );
            if (datacreate.id === undefined) {
                alert('Upload Failed')
            } else {
                Alert.alert('Success', 'Upload Success')
                Actions.pop()
                yield put(fetchCreateSuccess(datacreate))
            }


        } catch (err) {
            console.log('ERROR CREATE AGENT ', err);
            fetchCreateFinish(err)
        }
    }
}

export default function* wacher() {
    yield fork(createSaga);
}

