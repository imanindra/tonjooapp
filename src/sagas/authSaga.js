import { call, put, fork, take } from 'redux-saga/effects';
import { LOGIN, loginFinish, loginStart, loginSuccess } from '../redux/auth';
import { login } from '../services/api';
import { setErrorMessage } from '../redux/error';
import Storage from '../data/Storage';
import { Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';

function* loginSaga() {
    while (true) {
        const action = yield take(LOGIN);
        try {
            const { username, password } = action.payload;
            yield put(loginStart());
            const loginData = yield call(login, username, password);
            console.log(':DATTTTAAAAA', loginData)
            if (loginData) {
                Storage.setToken(loginData.token);
                Actions.Home()
                yield put(loginSuccess(loginData));
            } else {
                alert(JSON.stringify(loginData))
                yield put(loginFinish(loginData));
                yield put(setErrorMessage(loginData));
            }
        } catch (err) {
            alert('Username or password is wrong.')
            yield put(loginFinish('Login Gagal'));
            yield put(setErrorMessage('Login Gagal'));
        }
    }
}

export default function* wacher() {
    yield fork(loginSaga);
}
