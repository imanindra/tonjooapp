import { call, put, fork, take, cancel, select } from 'redux-saga/effects';
import { NativeModules, Platform } from 'react-native'
import { setAgentOffline, resetAgentOffline, SET_AGENTOFFLINE, finishAgentOffline } from '../../redux/offline/agent'
import { store } from '../../configureStore'
import { Actions } from 'react-native-router-flux';
import Storage from '../../data/Storage';

function* agentOfflineSaga() {
    while (true) {
        const action = yield take(SET_AGENTOFFLINE)
        try {
            const { data, dataOffline } = action.payload
            console.log('DATAOFFLINE', data)
            console.log('DATAOFFLINE', dataOffline)
            if (dataOffline === null) {
                yield put(setAgentOffline(data))
                Storage.setDataAgent(data)
                // yield put(resetTray({ data: [] }))
                Actions.pop()
            } else {
                dataOffline.push(data)
                yield put(setAgentOffline(dataOffline))
                Storage.setDataAgent(dataOffline)
                // yield put(resetTray({ data: [] }))
                Actions.pop()
            }
        } catch (err) {
            console.log("ERROR AGENT SAVE DATA", err)
            yield put(finishAgentOffline(err))
        }
    }
}

export default function* wacher() {
    yield fork(agentOfflineSaga)
}
