import { takeEvery, fork, all } from 'redux-saga/effects';
import authSaga from './authSaga';
import contactSaga from './contactSaga';
import createSaga from './createSaga';
import agentSaga from './offline/agentSaga'

export default function* rootSaga() {
    yield all([
        fork(authSaga),
        fork(contactSaga),
        fork(createSaga),
        fork(agentSaga)
    ]);
}
