import {Map, List, fromJS, Seq} from 'immutable';

export const LOGIN = 'auth/login';
export const LOGIN_START = 'auth/loginStart';
export const LOGIN_SUCCESS = 'auth/loginSuccess';
export const LOGIN_FINISH = 'auth/loginFinish';
export const LOGIN_STOP = 'auth/loginStop';

export function login(payload) {
  return {
    type: LOGIN,
    payload,
  };
}

export function loginStart() {
  return {
    type: LOGIN_START,
  };
}

export function loginSuccess(payload) {
  return {
    type: LOGIN_SUCCESS,
    payload,
  };
}

export function loginFinish(payload) {
  return {
    type: LOGIN_FINISH,
    payload,
  };
}

export function loginStop() {
  return {
    type: LOGIN_STOP,
  };
}

const initialState = Map({
  isFetching: false,
  loginSuccess: false,
  loginData: '',
  message: '',
});

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN_START:
      return state.set('isFetching', true);
    case LOGIN_SUCCESS:
      return state.withMutations(currentState =>
        currentState
          .set('isFetching', false)
          .set('loginData', action.payload)
          .set('loginSuccess', true),
      );
    case LOGIN_FINISH:
      return state.withMutations(currentState =>
        currentState
          .set('isFetching', false)
          .set('loginSuccess', false)
          .set('message', action.payload),
      );
    default:
      return state;
  }
}

export const getIsFetching = state => state.get('isFetching');
export const getIsSuccess = state => state.get('loginSuccess');
export const getLoginData = state => state.get('loginData');
export const getMessage = state => state.get('message');
