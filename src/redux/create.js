import { Map } from 'immutable';

export const FETCH_CREATE = 'create/fetchCreate';
export const FETCH_CREATE_START = 'create/fetchCreateStart';
export const FETCH_CREATE_SUCCESS = 'create/fetchCreateSuccess';
export const FETCH_CREATE_FINISH = 'create/fetchCreateFinish';


export function fetchCreate(payload) {
    return {
        type: FETCH_CREATE,
        payload,
    };
}

export function fetchCreateStart() {
    return {
        type: FETCH_CREATE_START,
    };
}

export function fetchCreateSuccess(payload) {
    return {
        type: FETCH_CREATE_SUCCESS,
        payload,
    };
}

export function fetchCreateFinish(payload) {
    return {
        type: FETCH_CREATE_FINISH,
        payload,
    };
}

const initialState = Map({
    isFetching: false,
    data: [],
    isSuccess: false,
});

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_CREATE_START:
            return state.set('isFetching', true);
        case FETCH_CREATE_SUCCESS:
            return state.withMutations(currentState =>
                currentState
                    .set('isFetching', false)
                    .set('isSuccess', true)
                    .set('data', action.payload),
            );
        case FETCH_CREATE_FINISH:
            return state.withMutations(currentState =>
                currentState
                    .set('isFetching', false)
                    .set('isSuccess', false),
            );
        default:
            return state;
    }
}

export const getIsFetching = state => state.get('isFetching');
export const getData = state => state.get('data');
export const getIsSuccess = state => state.get('isSuccess');
