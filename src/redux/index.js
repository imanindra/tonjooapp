import { combineReducers } from 'redux';
import authReducer from './auth';
import contactReducer from './contacts';
import createReducer from './create';

// OFFLINE
import agentReducer from './offline/agent'


const reducers = combineReducers({
    auth: authReducer,
    contact: contactReducer,
    create: createReducer,
    agent: agentReducer
});

export default reducers;
