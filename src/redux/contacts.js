import { Map } from 'immutable';

export const FETCH_CONTACTS = 'contacts/fetchContacts';
export const FETCH_CONTACTS_START = 'contacts/fetchContactsStart';
export const FETCH_CONTACTS_SUCCESS = 'contacts/fetchContactsSuccess';
export const FETCH_CONTACTS_FINISH = 'contacts/fetchContactsFinish';


export function fetchContacts(payload) {
    return {
        type: FETCH_CONTACTS,
        payload,
    };
}

export function fetchContactsStart() {
    return {
        type: FETCH_CONTACTS_START,
    };
}

export function fetchContactsSuccess(payload) {
    return {
        type: FETCH_CONTACTS_SUCCESS,
        payload,
    };
}

export function fetchContactsFinish(payload) {
    return {
        type: FETCH_CONTACTS_FINISH,
        payload,
    };
}

const initialState = Map({
    isFetching: false,
    data: [],
    isSuccess: false,
});

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_CONTACTS_START:
            return state.set('isFetching', true);
        case FETCH_CONTACTS_SUCCESS:
            return state.withMutations(currentState =>
                currentState
                    .set('isFetching', false)
                    .set('isSuccess', true)
                    .set('data', action.payload),
            );
        case FETCH_CONTACTS_FINISH:
            return state.withMutations(currentState =>
                currentState
                    .set('isFetching', false)
                    .set('isSuccess', false),
            );
        default:
            return state;
    }
}

export const getIsFetching = state => state.get('isFetching');
export const getData = state => state.get('data');
export const getIsSuccess = state => state.get('isSuccess');
