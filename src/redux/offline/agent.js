import { Map } from 'immutable'

export const SET_AGENTOFFLINE = "agent/setAgentOffline"
export const RESET_AGENTOFFLINE = "agent/resetAgentOffline"
export const AGENT_FINISHOFFLINE = "agent/finishAgentOffline";


export function setAgentOffline(payload) {
    return {
        type: SET_AGENTOFFLINE,
        payload
    }
}

export function resetAgentOffline() {
    return {
        type: RESET_AGENTOFFLINE
    }
}

export function finishAgentOffline(payload) {
    return {
        type: AGENT_FINISHOFFLINE,
        payload,
    };
}

const initialState = Map({
    data: []
})

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case SET_AGENTOFFLINE:
            return state.set('data', action.payload)
        case RESET_AGENTOFFLINE:
            return state.set('data', [])
        case AGENT_FINISHOFFLINE:
            return state.withMutations(currentState =>
                currentState
                    .set('isFetching', false)
                    .set('isSuccess', false),
            );
        default:
            return state
    }
}

export const getDataAgentOffline = state => state.get('data')
